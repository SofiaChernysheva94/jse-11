package ru.t1.chernysheva.tm.api.repository;

import ru.t1.chernysheva.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
